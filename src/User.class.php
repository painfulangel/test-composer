<?php namespace Mtigdemir\Blog;
// File must be under src directory
class User
{
    private $username;
    private $email;

    public function __constructor($username , $email){
        $this->username = $username;
        $this->email = $email;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getUsername(){
        return $this->username;
    }
}